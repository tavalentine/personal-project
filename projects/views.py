from django.shortcuts import render
from projects.models import Project


# Create your views here.


def list_projects(request):
    projects = Project.objects.all()  # Corrected from Project.object.all()
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)
