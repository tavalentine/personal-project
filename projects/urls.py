from projects.views import list_projects
from django.urls import path

urlpatterns = [
    path("", list_projects, name="list_projects"),
]
